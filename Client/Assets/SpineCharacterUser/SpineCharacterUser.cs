﻿using UnityEngine;
using Spine.Unity;

public class SpineCharacterUser : MonoBehaviour
{
    // ===============================================================
    // =                      Property [Editor]                      =
    // ===============================================================

    #region (功能) 提醒編輯者 Prefab 被改動了!
    [HideInInspector]
    // 沒意義的數值,這是為了讓自訂介面可以偵測到有數值更新的偷懶方法
    public bool Ini;
    #endregion
    
    // ===============================================================
    // =                          Setter                             =
    // ===============================================================

    [Header("【是否】顯示動畫名稱", order = 0)]
    public bool IsShowAnimNames;

    [Header("【是否】開啟時重置", order = 0)]
    public bool IsOnEnableReset = true;

    [Header("【多個】動畫資料", order = 0)]
    public MultipleAnimData MyMultipleAnimData = new MultipleAnimData();

    [Header("【是否】要循環播出", order = 0)]
    public bool IsLoop;

    [Header("【是否】打開時要播動畫", order = 0)]
    public bool IsOnEnablePlayAnim;

    [Header("【是否】打開要播動畫結束時要返回預設的動畫", order = 0)]
    public bool IsOnEnablePlayAnimAutoBackStartingAnim = true;

    [Header("【開始】動畫", order = 0)]
    public string OnEnableAnimName;

    [Header("【是否】使用間隔動畫", order = 0)]
    public bool IsUseIntervalAnim;

    [Header("【間隔】動畫最低時間", order = 0)]
    public float Time_IntervalAnimMin;

    [Header("【間隔】動畫最高時間", order = 0)]
    public float Time_IntervalAnimMax;

    [Header("【間隔】動畫名稱", order = 0)]
    public string IntervalAnimName;

    // ===============================================================
    // =                     Property [System]                       =
    // ===============================================================
    
    public SkeletonGraphic Spine { get { return GetComponent<SkeletonGraphic>(); } }
    
    // ===============================================================
    // =                     Property [Logic]                       =
    // ===============================================================

    /// <summary> 開始 動畫名稱 </summary>
    private string StartingAnimName;

    private string[] mAnimNames;
    /// <summary> 動畫們 (目前只用 Editor 會用到) </summary>
    public string[] AnimNames
    {
        get
        {
            // 取得 動畫們
            var anims = Spine.AnimationState.Data.SkeletonData.Animations.Items;
            // 設定 數量
            mAnimNames = new string[anims.Length];
            // 設定 動畫名稱們
            for (int i = 0; i < mAnimNames.Length; i++) { mAnimNames[i] = anims[i].Name; }
            // 回傳
            return mAnimNames;
        }
    }

    /// <summary> 結束 動畫名稱 </summary>
    private string EndAnimName;

    // ===============================================================
    // =                          System                             =
    // ===============================================================

    private void Awake()
    {
        StartingAnimName = Spine.startingAnimation;
    }

    private void Start()
    {
        // 間隔 動畫
        if (IsUseIntervalAnim) UseIntervalAnim();
    }

    private void OnEnable()
    {
        // 不需要 重置
        if (!IsOnEnableReset) return;
        // 客製化
        if (IsOnEnablePlayAnim)
        {
            // 返回 預設的動畫
            if (IsOnEnablePlayAnimAutoBackStartingAnim)
            {
                UseAndBackStartingAnim(OnEnableAnimName);
            }
            // 不返回
            else Use(OnEnableAnimName);
        }
        // 預設
        else Use(Spine.startingAnimation);
    }

    private void OnDestroy()
    {
        // 間隔 動畫
        if (IsUseIntervalAnim) CancelInvoke("UseIntervalAnim");
    }

    // ================================= Method [Use] =================================

    /// <summary> 使用 (動畫名稱) </summary>
    public void Use(string animName)
    {
        // 設定 動畫名稱
        Spine.startingAnimation = animName + "";
        // 設定 是否要循環播出
        Spine.startingLoop = IsLoop;
        // 初始化
        Spine.Initialize(true);
    }

    /// <summary> 使用 (開始動畫名稱) (結束動畫名稱) </summary>
    public void Use(string startAnimName, string endAnimName)
    {
        EndAnimName = endAnimName;
        Use(startAnimName);
        Spine.startingLoop = false;
        Spine.Initialize(true);
        // 結束時
        Spine.AnimationState.Complete += delegate
        {
            ReInvoke("UseEndAnim", 0.1f);
        };
    }

    /// <summary> 使用 並在結束時回到預設動畫 (動畫名稱) </summary>
    public void UseAndBackStartingAnim(string animName)
    {
        Use(animName);
        Spine.startingLoop = false;
        Spine.Initialize(true);
        // 結束時
        Spine.AnimationState.Complete += delegate
        {
            ReInvoke("StartingAnim", 0.1f);
        };
    }

    /// <summary> 停止 動畫 </summary>
    public void StopAnim()
    {
        Spine.AnimationState.ClearTracks();
    }

    /// <summary> 設定 造型 (造型名稱) </summary>
    public void SetSkin(string skinName)
    {
        Spine.initialSkinName = skinName + "";
    }

    /// <summary> 設定 是否循環播放 (是否循環播放) </summary>
    public void SetIsLoop(bool isLoop)
    {
        IsLoop = isLoop;
    }

    /// <summary> 設定 顏色 (顏色) </summary>
    public void SetColor(Vector4 Color) 
    {
        Spine.color = Color;
    }

    /// <summary> 取得 動畫時間 </summary>
    public float GetAnimTime()
    {
        return GetAnimTime(Spine.startingAnimation);
    }

    /// <summary> 取得 動畫時間 (動畫名稱) </summary>
    public float GetAnimTime(string animName)
    {
        foreach (var anim in Spine.SkeletonDataAsset.GetAnimationStateData().SkeletonData.Animations.Items)
        {
            if (anim.Name == animName) { return anim.Duration; }
        }
        Debug.LogError("[GetAnimTime Error] animName: " + animName);
        return -1;
    }

    // ===============================================================
    // =                           Method                            =
    // ===============================================================

    /// <summary> 使用 間隔動畫 (Invoke) </summary>
    private void UseIntervalAnim()
    {
        UseAndBackStartingAnim(IntervalAnimName);
        var time = Random.Range(Time_IntervalAnimMin, Time_IntervalAnimMax);
        ReInvoke("UseIntervalAnim", time);
    }

    /// <summary> 使用 開始動畫 (Invoke) </summary>
    public void StartingAnim()
    {
        Use(StartingAnimName);
    }
    
    /// <summary> 使用 結束動畫 (Invoke) </summary>
    public void UseEndAnim()
    {
        Use(EndAnimName);
    }

    private void ReInvoke(string methodName, float delay)
    {
        CancelInvoke(methodName);
        Invoke(methodName, delay);
    }

    private void ReInvokeRepeating(string methodName, float delay, float repeatRate)
    {
        CancelInvoke(methodName);
        InvokeRepeating(methodName, delay, repeatRate);
    }
    
    // ===============================================================
    // =                            Class                            =
    // ===============================================================

    /// <summary> 多個 動畫資料 </summary>
    [System.Serializable]
    public class MultipleAnimData
    {
        // 開始 動畫名稱
        public string startAnimName;
        // 結束 動畫名稱
        public string endAnimName;
    }
}