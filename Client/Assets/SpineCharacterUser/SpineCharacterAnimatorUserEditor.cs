﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace Gamble
{
    // 設定 要繪製的腳本
    [CustomEditor(typeof(SpineCharacterAnimatorUser))]

    public class SpineCharacterAnimatorUserEditor : Editor
    {
        // ===============================================================
        // =                      Property [Editor]                      =
        // ===============================================================

        #region (功能) 提醒編輯者 Prefab 被改動了!
        // 讀取資料
        // (正確的作法是每個數值都要通過這個來判斷來確定是否有修改,有修改會跳出是否儲存介面,
        // 但是我現在用偷懶的方式來做,只要任何其他參數變更,
        // 就自動把這數值做修改讓系統判斷需要儲存資料,這樣做可以省去很多時間在寫讀取資料)
        SerializedProperty m_Ini;
        int[] index;//目前選擇的標記目標

        // 開啟這物件就初始化一次,在自訂編輯器的初始化,必須放在OnEnable
        public void OnEnable()
        {
            m_Ini = serializedObject.FindProperty("Ini");
        }
        #endregion

        // ===============================================================
        // =                          Property                           =
        // ===============================================================

        #region Property [Title]
        // 宣告 每一項元素的標題
        private string Title;
        #endregion

        #region Property [MyScript]
        // 宣告 要繪製的腳本
        private SpineCharacterAnimatorUser MyScript;
        #endregion

        // ===============================================================
        // =                          System                             =
        // ===============================================================

        // 繪製 面板
        public override void OnInspectorGUI()
        {
            // 設定 要繪製的腳本
            MyScript = (SpineCharacterAnimatorUser)target;

            #region (功能) 提醒編輯者 Prefab 被改動了!
            // (此功能能提醒編輯者，有 Prefab 被改動了!)
            // 如果有參數變更
            if (GUI.changed)
            {
                // 從物體上抓取最新的數據。
                // serializedObject = 目前選擇到的物件。
                serializedObject.Update();

                if (m_Ini.boolValue == true)
                {
                    m_Ini.boolValue = false;
                    serializedObject.ApplyModifiedProperties();
                }
                else
                {
                    m_Ini.boolValue = true;
                    serializedObject.ApplyModifiedProperties();
                }
            }
            #endregion

            // 顯示 標題
            ShowTitle("【Spine】角色動畫管理器");

            #region 開始垂直組
            GUILayout.BeginVertical("box");
            // 開始垂直組
            GUILayout.Label("", "MeTransOnRight");
            #endregion

            // 顯示 純粹標題
            ShowPureTitle("【設定】");

            // --------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.BeginVertical("box");
            GUILayout.Label("【基礎】", "U2D.createRect");
            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("AnimDatas"), true);
            serializedObject.ApplyModifiedProperties();
            GUILayout.EndVertical();
            // --------------------------------------------------------------------------------------------------------------------------------------
            
            // --------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.BeginVertical("box");
            GUILayout.Label("【動畫名稱列表】", "U2D.createRect");
            var context = string.Empty;
            if (MyScript.AnimNames != null)
            {
                foreach (var animName in MyScript.AnimNames)
                {
                    context += animName + "\n";
                }
            }
            EditorGUILayout.TextArea(context);
            GUILayout.EndVertical();
            GUILayout.Space(10);
            // --------------------------------------------------------------------------------------------------------------------------------------
            
            // --------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.BeginVertical("box");
            GUILayout.Label("【預覽】", "U2D.createRect");
            // 運行中
            if (EditorApplication.isPlaying)
            {
                ShowStringArrayByButton("動畫列表", MyScript.AnimNames);
            }
            // 尚未 運行
            else { GUILayout.TextField("【Unity運行狀態下即可預覽】"); }
            GUILayout.EndVertical();
            
            GUILayout.Space(10);
            // --------------------------------------------------------------------------------------------------------------------------------------

            #region 結束垂直組
            GUILayout.Label("", "MeTransOnRight");
            // 結束垂直組
            GUILayout.EndVertical();
            #endregion

            #region (功能) 提醒編輯者 Prefab 被改動了!
            // (此功能能提醒編輯者，有 Prefab 被改動了!)
            // 如果有參數變更
            if (GUI.changed)
            {
                // 從物體上抓取最新的數據。
                // serializedObject = 目前選擇到的物件。
                serializedObject.Update();

                if (m_Ini.boolValue == true)
                {
                    m_Ini.boolValue = false;
                    serializedObject.ApplyModifiedProperties();
                }
                else
                {
                    m_Ini.boolValue = true;
                    serializedObject.ApplyModifiedProperties();
                }
            }
            #endregion
        }

        // ===============================================================
        // =                           Method                            =
        // ===============================================================

        /// <summary> 顯示 字串陣列按鈕 (陣列名稱) (字串陣列) </summary>
        private void ShowStringArrayByButton(string arrayName, string[] array)
        {
            // 開始 垂直組
            GUILayout.BeginVertical("box");
            // 顯示 標題
            GUILayout.Label(arrayName);
            // 顯示
            foreach (var info in array) { CreateAnimUseButton(info); }
            // 結束 垂直組
            GUILayout.EndVertical();
        }
        
        /// <summary> 建造 動畫按鈕 (按鈕名稱) </summary>
        private void CreateAnimUseButton(string buttonName)
        {
            // 按鈕
            if (GUILayout.Button("【" + buttonName + "】", "LargeButton"))
            {
                // 使用 (動畫名稱)
                MyScript.Use(buttonName, MyScript.Test_IsLoop);
            }
        }
        
        /// <summary> 顯示 純粹標題 </summary>
        private void ShowPureTitle(string title)
        {
            #region 大標題 
            // 大標題
            GUILayout.Space(20);
            GUI.skin.label.fontSize = 24;
            GUI.skin.label.alignment = TextAnchor.MiddleCenter;
            GUILayout.Label(title);
            GUI.skin.label.fontSize = 12;
            GUI.skin.label.alignment = TextAnchor.MiddleLeft;
            GUILayout.Space(20);
            #endregion
        }

        /// <summary> 顯示 標題 </summary>
        private void ShowTitle(string title)
        {
            #region 開始垂直組
            // 開始 垂直組
            GUILayout.BeginVertical("box");
            // 框框
            GUILayout.Label("", "MeTransOnRight");
            #endregion

            #region 大標題 
            // 大標題
            GUILayout.Space(20);
            GUI.skin.label.fontSize = 24;
            GUI.skin.label.alignment = TextAnchor.MiddleCenter;
            GUILayout.Label(title);
            GUI.skin.label.fontSize = 12;
            GUI.skin.label.alignment = TextAnchor.MiddleLeft;
            GUILayout.Space(20);
            #endregion

            #region 結束垂直組
            // 框框
            GUILayout.Label("", "MeTransOnRight");
            // 結束 垂直組
            GUILayout.EndVertical();
            #endregion
        }
    }
}
#endif