﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace UITheme
{
    // 設定 要繪製的腳本
    [CustomEditor(typeof(SpineCharacterUser))]

    public class SpineCharacterUserEditor : Editor
    {
        // ===============================================================
        // =                      Property [Editor]                      =
        // ===============================================================

        #region (功能) 提醒編輯者 Prefab 被改動了!
        // 讀取資料
        // (正確的作法是每個數值都要通過這個來判斷來確定是否有修改,有修改會跳出是否儲存介面,
        // 但是我現在用偷懶的方式來做,只要任何其他參數變更,
        // 就自動把這數值做修改讓系統判斷需要儲存資料,這樣做可以省去很多時間在寫讀取資料)
        SerializedProperty m_Ini;
        int[] index;//目前選擇的標記目標

        // 開啟這物件就初始化一次,在自訂編輯器的初始化,必須放在OnEnable
        public void OnEnable()
        {
            m_Ini = serializedObject.FindProperty("Ini");
        }
        #endregion

        // ===============================================================
        // =                          Property                           =
        // ===============================================================

        private string Title;

        private SpineCharacterUser MyScript;

        // ===============================================================
        // =                          System                             =
        // ===============================================================

        public override void OnInspectorGUI()
        {
            // 設定 要繪製的腳本
            MyScript = (SpineCharacterUser)target;

            #region (功能) 提醒編輯者 Prefab 被改動了!
            // (此功能能提醒編輯者，有 Prefab 被改動了!)
            // 如果有參數變更
            if (GUI.changed)
            {
                // 從物體上抓取最新的數據。
                // serializedObject = 目前選擇到的物件。
                serializedObject.Update();

                if (m_Ini.boolValue == true)
                {
                    m_Ini.boolValue = false;
                    serializedObject.ApplyModifiedProperties();
                }
                else
                {
                    m_Ini.boolValue = true;
                    serializedObject.ApplyModifiedProperties();
                }
            }
            #endregion

            // 顯示 標題
            ShowTitle("【Spine角色控制器】");

            #region 開始垂直組
            GUILayout.BeginVertical("box");
            // 開始垂直組
            GUILayout.Label("", "MeTransOnRight");
            #endregion
            
            // 顯示 純粹標題
            ShowPureTitle("【設定】");

            // --------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.BeginVertical("box");
            GUILayout.Label("【基礎】", "U2D.createRect");
            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("IsOnEnableReset"), true);
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("IsLoop"), true);
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("IsOnEnablePlayAnim"), true);
            serializedObject.ApplyModifiedProperties();
            if (MyScript.IsOnEnablePlayAnim)
            {
                serializedObject.Update();
                EditorGUILayout.PropertyField(serializedObject.FindProperty("OnEnableAnimName"), true);
                serializedObject.ApplyModifiedProperties();
                serializedObject.Update();
                EditorGUILayout.PropertyField(serializedObject.FindProperty("IsOnEnablePlayAnimAutoBackStartingAnim"), true);
                serializedObject.ApplyModifiedProperties();  
            }
            
            GUILayout.Space(10);

            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("IsUseIntervalAnim"), true);
            serializedObject.ApplyModifiedProperties();
            if (MyScript.IsUseIntervalAnim)
            {
                serializedObject.Update();
                EditorGUILayout.PropertyField(serializedObject.FindProperty("Time_IntervalAnimMin"), true);
                serializedObject.ApplyModifiedProperties();
                serializedObject.Update();
                EditorGUILayout.PropertyField(serializedObject.FindProperty("Time_IntervalAnimMax"), true);
                serializedObject.ApplyModifiedProperties();
                serializedObject.Update();
                EditorGUILayout.PropertyField(serializedObject.FindProperty("IntervalAnimName"), true);
                serializedObject.ApplyModifiedProperties();
            }
            
            GUILayout.EndVertical();
            // --------------------------------------------------------------------------------------------------------------------------------------

            // // 顯示 純粹標題
            // ShowPureTitle("【單個動畫】");
            //
            // // 設定 
            // MyScript.IsOnEnableReset = EditorGUILayout.Toggle("開啟時重置", MyScript.IsOnEnableReset);
            // // 設定 
            // MyScript.IsLoop = EditorGUILayout.Toggle("循環播出", MyScript.IsLoop);
            //
            // GUILayout.Space(10);
            //
            // // 設定 
            // MyScript.IsOnEnablePlayAnim = EditorGUILayout.Toggle("客製化開啟時動畫", MyScript.IsOnEnablePlayAnim);
            // // 需要
            // if (MyScript.IsOnEnablePlayAnim)
            // {
            //     MyScript.OnEnableAnimName = EditorGUILayout.TextField("開啟時的動畫名稱", MyScript.OnEnableAnimName);
            //     // 設定 
            //     MyScript.IsOnEnablePlayAnimAutoBackStartingAnim = EditorGUILayout.Toggle("返回預設的動畫", MyScript.IsOnEnablePlayAnimAutoBackStartingAnim);
            // }

           
            
            // // 設定 
            // MyScript.IsUseIntervalAnim = EditorGUILayout.Toggle("使用間隔動畫", MyScript.IsUseIntervalAnim);
            // // 需要
            // if (MyScript.IsUseIntervalAnim)
            // {
            //     GUILayout.Label("每隔 [最低時間] - [最高時間] 播一次"); 
            //     MyScript.Time_IntervalAnimMin = EditorGUILayout.FloatField("最低時間", MyScript.Time_IntervalAnimMin);
            //     MyScript.Time_IntervalAnimMax = EditorGUILayout.FloatField("最高時間", MyScript.Time_IntervalAnimMax);
            //     MyScript.IntervalAnimName = EditorGUILayout.TextField("動畫名稱", MyScript.IntervalAnimName);
            // }

            // #region 結束垂直組
            // GUILayout.Label("", "MeTransOnRight");
            // // 結束垂直組
            // GUILayout.EndVertical();
            // #endregion

            // #region 開始垂直組
            // GUILayout.BeginVertical("box");
            // // 開始垂直組
            // GUILayout.Label("", "MeTransOnRight");
            // #endregion

            // 顯示 純粹標題
            ShowPureTitle("【單個動畫】");

            // --------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.BeginVertical("box");
            GUILayout.Label("【表現】", "U2D.createRect");
            serializedObject.Update();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("IsShowAnimNames"), true);
            serializedObject.ApplyModifiedProperties();
            // 運行中
            if (EditorApplication.isPlaying)
            {
                // 顯示
                if (MyScript.IsShowAnimNames) { ShowStringArrayByButton("動畫列表", MyScript.AnimNames); }
            }
            // 尚未 運行
            else { GUILayout.TextField("【Unity運行狀態下即可預覽】"); }
            GUILayout.EndVertical();
            
            GUILayout.Space(10);
            // --------------------------------------------------------------------------------------------------------------------------------------
            
            // // 設定 
            // MyScript.IsShowAnimNames = EditorGUILayout.Toggle("顯示動畫列表", MyScript.IsShowAnimNames);

            // // 運行中
            // if (EditorApplication.isPlaying)
            // {
            //     // 顯示
            //     if (MyScript.IsShowAnimNames) { ShowStringArrayByButton("動畫列表", MyScript.AnimNames); }
            // }
            // // 尚未 運行
            // else { GUILayout.TextField("【Unity運行狀態下即可預覽】"); }

            // #region 結束垂直組
            // GUILayout.Label("", "MeTransOnRight");
            // // 結束垂直組
            // GUILayout.EndVertical();
            // #endregion

            // #region 開始垂直組
            // GUILayout.BeginVertical("box");
            // // 開始垂直組
            // GUILayout.Label("", "MeTransOnRight");
            // #endregion

            // 顯示 純粹標題
            ShowPureTitle("【做完一個動作後，做另一個動作】");
            
            // --------------------------------------------------------------------------------------------------------------------------------------
            GUILayout.BeginVertical("box");
            GUILayout.Label("【表現】", "U2D.createRect");
            
            // 顯示 動畫清單
            ShowAnimList();

            // 空格
            GUILayout.Space(10);

            // 設定 
            MyScript.MyMultipleAnimData.startAnimName = EditorGUILayout.TextField("[開始] 動畫名稱", MyScript.MyMultipleAnimData.startAnimName);
            // 設定 
            MyScript.MyMultipleAnimData.endAnimName = EditorGUILayout.TextField("[結束] 動畫名稱", MyScript.MyMultipleAnimData.endAnimName);

            // 運行中
            if (EditorApplication.isPlaying)
            {
                // 按鈕
                if (GUILayout.Button("【預覽】", "LargeButton"))
                {
                    // 使用 (開始動畫名稱) (結束動畫名稱)
                    MyScript.Use(MyScript.MyMultipleAnimData.startAnimName, MyScript.MyMultipleAnimData.endAnimName);
                }
            }
            // 尚未 運行
            else { GUILayout.TextField("【Unity運行狀態下即可預覽】"); }
            GUILayout.EndVertical();
            
            GUILayout.Space(10);
            // --------------------------------------------------------------------------------------------------------------------------------------

            #region 結束垂直組
            GUILayout.Label("", "MeTransOnRight");
            // 結束垂直組
            GUILayout.EndVertical();
            #endregion

            #region (功能) 提醒編輯者 Prefab 被改動了!
            // (此功能能提醒編輯者，有 Prefab 被改動了!)
            // 如果有參數變更
            if (GUI.changed)
            {
                // 從物體上抓取最新的數據。
                // serializedObject = 目前選擇到的物件。
                serializedObject.Update();

                if (m_Ini.boolValue == true)
                {
                    m_Ini.boolValue = false;
                    serializedObject.ApplyModifiedProperties();
                }
                else
                {
                    m_Ini.boolValue = true;
                    serializedObject.ApplyModifiedProperties();
                }
            }
            #endregion
        }

        // =================================== Method =====================================

        #region Method ShowPureTitle
        // 顯示 純粹標題
        private void ShowPureTitle(string title)
        {
            #region 大標題 
            // 大標題
            GUILayout.Space(20);
            GUI.skin.label.fontSize = 24;
            GUI.skin.label.alignment = TextAnchor.MiddleCenter;
            GUILayout.Label(title);
            GUI.skin.label.fontSize = 12;
            GUI.skin.label.alignment = TextAnchor.MiddleLeft;
            GUILayout.Space(20);
            #endregion
        }
        #endregion

        #region Method ShowTitle
        // 顯示 標題
        private void ShowTitle(string title)
        {
            #region 開始垂直組
            // 開始 垂直組
            GUILayout.BeginVertical("box");
            // 框框
            GUILayout.Label("", "MeTransOnRight");
            #endregion

            #region 大標題 
            // 大標題
            GUILayout.Space(20);
            GUI.skin.label.fontSize = 24;
            GUI.skin.label.alignment = TextAnchor.MiddleCenter;
            GUILayout.Label(title);
            GUI.skin.label.fontSize = 12;
            GUI.skin.label.alignment = TextAnchor.MiddleLeft;
            GUILayout.Space(20);
            #endregion

            #region 結束垂直組
            // 框框
            GUILayout.Label("", "MeTransOnRight");
            // 結束 垂直組
            GUILayout.EndVertical();
            #endregion
        }
        #endregion

        #region Method ShowStringArrayByButton
        // 顯示 字串陣列按鈕 (陣列名稱) (字串陣列)
        private void ShowStringArrayByButton(string arrayName, string[] array)
        {
            // 開始 垂直組
            GUILayout.BeginVertical("box");
            // 顯示 標題
            GUILayout.Label(arrayName);
            // 顯示
            foreach (var info in array) { CreateAnimUseButton(info); }
            // 結束 垂直組
            GUILayout.EndVertical();
        }
        #endregion

        #region Method CreateAnimUseButton
        // 建造 動畫按鈕 (按鈕名稱)
        private void CreateAnimUseButton(string buttonName)
        {
            // 按鈕
            if (GUILayout.Button("【" + buttonName + "】", "LargeButton"))
            {
                // 使用 (動畫名稱)
                MyScript.Use(buttonName);
            }
        }
        #endregion

        #region Method ShowAnimList
        // 顯示 動畫清單
        private void ShowAnimList()
        {
            // 取得 資訊
            var info = "[動畫清單]";
            // 動畫 名稱
            for (int i = 0; i < MyScript.AnimNames.Length; i++) { info += "\n" + MyScript.AnimNames[i]; }
            // 文字 區塊
            EditorGUILayout.TextArea(info);
        }
        #endregion
    }
}
#endif