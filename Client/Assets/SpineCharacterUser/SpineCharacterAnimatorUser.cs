﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;

/// <summary> Spine 角色動畫管理器 </summary>
public class SpineCharacterAnimatorUser : MonoBehaviour
{
    // ===============================================================
    // =                      Property [Editor]                      =
    // ===============================================================

    #region (功能) 提醒編輯者 Prefab 被改動了!
    [HideInInspector]
    // 沒意義的數值,這是為了讓自訂介面可以偵測到有數值更新的偷懶方法
    public bool Ini;
    #endregion
    
    [Header("【測試】是否循環", order = 0)]
    public bool Test_IsLoop;
    
    // ===============================================================
    // =                          Setter                             =
    // ===============================================================
    
    [Header("【動畫】資料們", order = 0)]
    public CAnimData[] AnimDatas;
    
    // ===============================================================
    // =                     Property [System]                       =
    // ===============================================================
    
    private SkeletonGraphic mMySpine;
    /// <summary> 我的 Spine </summary>
    public SkeletonGraphic MySpine
    {
        get
        {
            if (mMySpine == null) mMySpine = GetComponent<SkeletonGraphic>();
            if (mMySpine == null) mMySpine = GetComponentInParent<SkeletonGraphic>();
            return mMySpine;
        }
    }

    // ===============================================================
    // =                            Logic                            =
    // ===============================================================

    private string[] mAnimNames;
    /// <summary> 動畫們 (目前只用 Editor 會用到) </summary>
    public string[] AnimNames
    {
        get
        {
            if (MySpine == null || MySpine.skeletonDataAsset == null) return mAnimNames;
            // 取得 動畫們
            var anims = MySpine.AnimationState.Data.SkeletonData.Animations.Items;
            Debug.Log($"anims.Length: {anims.Length}");
            // 設定 數量
            mAnimNames = new string[anims.Length];
            // 設定 動畫名稱們
            for (int i = 0; i < mAnimNames.Length; i++) { mAnimNames[i] = anims[i].Name; }
            // 回傳
            return mAnimNames;
        }
    }
    
    /// <summary> 目前準備中 的動畫資料 </summary>
    private CAnimData NowReadyAnimData;

    /// <summary> 完成 事件 </summary>
    public Action CompleteAction;
    
    /// <summary> 是否 已加入完成事件 </summary>
    private bool IsAddCompleteAction;

    /// <summary> 是否 正在使用準備動畫 </summary>
    private bool IsUsingReadyAnim;
    
    private Coroutine _coroutineSetCompleteAction;
    
    // ===============================================================
    // =                          System                             =
    // ===============================================================

    private void Start()
    {
        MySpine.AnimationState.Complete += delegate { CompleteAction?.Invoke(); };
    }

    private void OnEnable()
    {
        if (AnimDatas == null || AnimDatas.Length == 0) return;
        var data = Array.Find(AnimDatas, x => x.IsDefault);
        // 預設 動畫
        if (data != null) UseAnim(data);
        // 使用 第一個
        else if (AnimDatas.Length > 0) UseDefaultAnimProcess();
    }
    
    // ===============================================================
    // =                         Method [Use]                        =
    // ===============================================================

    /// <summary> 使用 預設動畫流程 </summary>
    public void UseDefaultAnimProcess()
    {
        CancelDefaultAnimProcess();
        UseAnim(AnimDatas[0]);
    }

    /// <summary> 取消 使用預設動畫流程 </summary>
    public void CancelDefaultAnimProcess()
    {
        CancelInvoke("UseReadyAnim");
        IsUsingReadyAnim = false;
        CompleteAction = null;
    }
    
    /// <summary> 使用 目前準備中的動畫資料 </summary>
    public void UseReadyAnim()
    {
        UseAnim(NowReadyAnimData);
        IsUsingReadyAnim = false;
    }
    
    /// <summary> 使用 (動畫名稱) (是否循環) (完成事件) </summary>
    public void Use(string animName, bool isLoop, Action completeAction)
    {
        Use(animName, isLoop);
        CompleteAction = null;
        if (_coroutineSetCompleteAction != null) StopCoroutine(_coroutineSetCompleteAction);
        _coroutineSetCompleteAction = StartCoroutine(SetCompleteAction(completeAction));
    }
    
    /// <summary> 使用 沒有延遲設定完成事件 (動畫名稱) (是否循環) (完成事件) </summary>
    public void UseNoDelaySetAction(string animName, bool isLoop, Action completeAction)
    {
        Use(animName, isLoop);
        CompleteAction = completeAction;
    }

    /// <summary> 使用 (動畫名稱) </summary>
    public void Use(string animName) => Use(animName, false);

    /// 加速
    public void SetTimeScale(float value) { MySpine.timeScale = value; }
    
    /// <summary> 使用 不需要過渡 (動畫名稱) </summary>
    public void UseNoBlending(string animName) => Use(animName, false, false);

    /// <summary> 使用 (動畫名稱) (是否循環) (是否要使用過渡) </summary>
    public void Use(string animName, bool isLoop, bool blending = true)
    {
        if (MySpine == null) return;
        MySpine.startingAnimation = animName + "";
        MySpine.startingLoop = isLoop;
        var animationState = MySpine.AnimationState; 
        if (animationState != null)
        {
            var entry = animationState.SetAnimation(0, $"{animName}", isLoop);
            if (!blending) entry.MixDuration = 0;    
        }
    }
    
    /// <summary> 使用 (開始動畫名稱) (結束動畫名稱) (結束動畫是否要循環) </summary>
    public void Use(string startAnimName, string endAnimName, bool isEndAnimLoop)
    {
        Use(startAnimName, false, () =>
        {
            Use(endAnimName, isEndAnimLoop);
            CompleteAction = null;
        });
    }
    
    /// <summary> 使用 動畫 (動畫資料) </summary>
    public void UseAnim(CAnimData animData)
    {
        UseNoDelaySetAction(animData.AnimName, animData.IsLoop, () =>
        {
            if (IsUsingReadyAnim) return;
            if (!string.IsNullOrEmpty(animData.EndAnimName))
            {
                UseAnim(Array.Find(AnimDatas, x => x.AnimName == animData.EndAnimName));
                return;
            }
            if (!string.IsNullOrEmpty(animData.RandomAnimName))
            {
                var delay = UnityEngine.Random.Range(animData.RandomAnimTimeMin, animData.RandomAnimTimeMax);
                NowReadyAnimData = Array.Find(AnimDatas, x => x.AnimName == animData.RandomAnimName);
                ReInvoke("UseReadyAnim", delay);
                IsUsingReadyAnim = true;
                return;
            }
        });
    }
    
    /// <summary> 設定 造型 (造型名稱) </summary>
    public void SetSkin(string skinName)
    {
        MySpine.initialSkinName = skinName;
        if (MySpine.Skeleton != null)
        {
            MySpine.Skeleton.SetSkin(skinName);
            MySpine.Skeleton.SetSlotsToSetupPose();
        }
    }
    
    /// <summary> 設定 顏色 (顏色) </summary>
    public void SetColor(Color color) => MySpine.color = color;
    
    /// <summary> 停止 動畫 </summary> (這個在HotFix裡，會有問題，先不使用。)
    public void StopAnim() => MySpine.AnimationState.ClearTracks();

    /// <summary> 取得 動畫名稱 </summary>
    public string GetAnimName() => MySpine.startingAnimation;
    
    /// <summary> 取得 動畫時間 </summary>
    public float GetAnimTime() => GetAnimTime(MySpine.startingAnimation);

    /// <summary> 取得 動畫時間 (動畫名稱) </summary>
    public float GetAnimTime(string animName)
    {
        foreach (var anim in MySpine.SkeletonDataAsset.GetAnimationStateData().SkeletonData.Animations.Items)
        {
            if (anim.Name == animName) return anim.Duration;
        }
        Debug.LogError("[GetAnimTime Error] animName: " + animName);
        return -1;
    }

    // ===============================================================
    // =                           Method                            =
    // ===============================================================
    
    /// <summary> 設定 完成事件 (完成事件) </summary>
    private IEnumerator SetCompleteAction(Action completeAction)
    {
        // 當上一個動畫是 Loop 的 Idle 時，會平凡的觸發 Complete。
        // 有時會在換動畫時，還觸發到上一個動畫的 Spine 的 Complete。
        // 因此做了小延遲的註冊事件。
        yield return new WaitForSeconds(0.5f);
        CompleteAction = completeAction;
    }
    
    private void ReInvoke(string methodName, float delay)
    {
        CancelInvoke(methodName);
        Invoke(methodName, delay);
    }
    
    // ===============================================================
    // =                            Class                            =
    // ===============================================================
    
    [Serializable]
    // 動畫 資料
    public class CAnimData
    {
        // 別改名 prefab設定 會跑掉。
        [Header("【是否】為預設動畫", order = 0)] public bool IsDefault;
        [Header("【是否】循環", order = 0)] public bool IsLoop;
        [Header("【動畫】名稱", order = 0)] public string AnimName;
        [Header("【結束】動畫名稱", order = 0)] public string EndAnimName;
        [Header("【隨機】動畫名稱", order = 0)] public string RandomAnimName;
        [Header("【隨機】動畫時間最小值", order = 0)] public float RandomAnimTimeMin;
        [Header("【隨機】動畫時間最大值", order = 0)] public float RandomAnimTimeMax;
    }
}