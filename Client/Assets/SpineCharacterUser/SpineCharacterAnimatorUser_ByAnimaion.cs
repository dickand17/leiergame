﻿using System;
using UnityEngine;

/// <summary> Spine 角色動畫管理器 </summary>
public class SpineCharacterAnimatorUser_ByAnimaion : MonoBehaviour
{
    // ===============================================================
    // =                      Property [Editor]                      =
    // ===============================================================

    #region (功能) 提醒編輯者 Prefab 被改動了!
    [HideInInspector]
    // 沒意義的數值,這是為了讓自訂介面可以偵測到有數值更新的偷懶方法
    public bool Ini;
    #endregion

    // ===============================================================
    // =                          Setter                             =
    // ===============================================================
    
    [Header("【動畫】資料們", order = 0)]
    public CAnimData[] AnimDatas;
    
    // ===============================================================
    // =                     Property [System]                       =
    // ===============================================================
    
    // private SpineCharacterAnimatorUser mMySpine;
    // /// <summary> 我的 Spine </summary>
    // public SpineCharacterAnimatorUser MySpine
    // {
    //     get
    //     {
    //         if (mMySpine == null) mMySpine = GetComponent<SpineCharacterAnimatorUser>();
    //         if (mMySpine == null) mMySpine = GetComponentInParent<SpineCharacterAnimatorUser>();
    //         return mMySpine;
    //     }
    // }
    
    // private Animator mAnimator;
    // /// <summary> 我的 Spine </summary>
    // public Animator MyAnimator
    // {
    //     get
    //     {
    //         if (mAnimator == null) mAnimator = GetComponent<Animator>();
    //         return mAnimator;
    //     }
    // }

    // ===============================================================
    // =                            Logic                            =
    // ===============================================================

    // /// <summary> 是否 正在使用準備動畫 </summary>
    // private bool IsUsingReadyAnim;
    
    // ===============================================================
    // =                          System                             =
    // ===============================================================

    // ===============================================================
    // =                         Method [Use]                        =
    // ===============================================================
    
    /// <summary> 使用 (Animation名稱) </summary>
    public void Use(string animationName)
    {
        var data = GetCAnimData(animationName);
        data.SpineCharacterAnimatorUser.UseNoBlending(data.SpineAnimName);
    }

    // ===============================================================
    // =                           Method                            =
    // ===============================================================

    /// <summary> 取得 動畫資料 (Animation名稱) </summary>
    public CAnimData GetCAnimData(string animationName)
    {
        return Array.Find(AnimDatas, x => x.AnimationName == animationName);
    }
    
    // ===============================================================
    // =                            Class                            =
    // ===============================================================
    
    [Serializable]
    // 動畫 資料
    public class CAnimData
    {
        [Header("【Animation】名稱", order = 0)]
        public string AnimationName;
        
        [Header("【Spine】角色動畫管理器", order = 0)]
        public SpineCharacterAnimatorUser SpineCharacterAnimatorUser;
            
        [Header("【Spine】動畫名稱", order = 0)]
        public string SpineAnimName;
    }
}