﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SpineDialogUser : MonoBehaviour
{
    // =============================== Property [Layout] ==============================

    [Header("=============================================================================================", order = 0)]
    [Header("【設定】對話文字", order = 1)]
    public Text Text_Content;
    [Header("=============================================================================================", order = 0)]
    [Header("【設定】會說哪些話", order = 1)]
    public List<string> List_Content;
    [Header("=============================================================================================", order = 0)]
    [Header("【設定】對話框顯示時間", order = 1)]
    public float ShowTime;
    [Header("=============================================================================================", order = 0)]
    [Header("【設定】是否 Awake 會執行", order = 1)]
    public bool IsAwake_Init;
    [Header("=============================================================================================", order = 0)]
    [Header("【設定】初始化事件", order = 1)]
    public UnityEvent Event_Init;
    [Header("=============================================================================================", order = 0)]
    [Header("【設定】使用時的事件", order = 1)]
    public UnityEvent Event_Use;
    [Header("=============================================================================================", order = 0)]
    [Header("【設定】不使用時的事件", order = 1)]
    public UnityEvent Event_UnUse;

    // ==================================== System ====================================

    #region System Awake
    private void Awake()
    {
        // 初始化
        if (IsAwake_Init) { Init(); }
    }
    #endregion

    // ==================================== Method ====================================

    #region Method Init
    // 使用 
    public void Init() { Event_Init.Invoke(); }
    #endregion

    #region Method Use
    // 使用 
    public void Use()
    {
        // 設定 對話內容
        SetText_Content();
        // 使用 流程
        UseProcess();
    }
    #endregion

    #region Method Use
    // 使用 (對話內容)
    public void Use(string context)
    {
        // 設定 對話內容
        Text_Content.text = context;
        // 使用 流程
        UseProcess();        
    }
    #endregion

    #region Method UnUse
    // 不使用 
    public void UnUse() { Event_UnUse.Invoke(); }
    #endregion

    // ================================ Method [System] ===============================

    #region Method UseProcess
    // 使用 流程
    private void UseProcess()
    {
        // 執行
        Event_Use.Invoke();
        // 不使用 
        RestartInvoke("UnUse", ShowTime);
    }
    #endregion

    // =============================== Method [Invoke] ================================

    #region Method RestartInvoke
    // 重新 啟用 Invoke (Invoke 名稱) (多久後使用)
    public void RestartInvoke(string invokeName, float delay) { CancelInvoke(invokeName); Invoke(invokeName, delay); }
    #endregion

    // =============================== Method [Content] ===============================

    #region Method SetText_Content
    // 設定 對話內容
    private void SetText_Content() { Text_Content.text = RandomContent(); }
    #endregion

    #region Method RandomContent
    // 亂數 對話內容
    private string RandomContent()
    {
        // 沒內容
        if (List_Content.Count <= 0) { Debug.Log("請提供對話內容"); return "請提供對話內容"; }
        // 流水號
        var index = Random.Range(0, List_Content.Count);
        // 內容
        return List_Content[index];
    }
    #endregion
}
