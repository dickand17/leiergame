﻿using Spine;
using Spine.Unity;
using UnityEngine;

namespace FlySky.Tool
{
    public class SpineUser : MonoBehaviour
    {
        private SkeletonAnimation spine;
        private Skeleton Skeleton => spine.skeleton;
        private Spine.AnimationState State => spine.state;

        public void Awake() => spine = GetComponent<SkeletonAnimation>();
        
        public void SetAnimation(string aniName, bool loop)
        {
            if (Skeleton.Data.FindAnimation(aniName) == null)
            {
                Debug.Log(name);
                return;            
            }
            State.Start += Handle;
            State.SetAnimation(0, aniName, loop);
            State.Start -= Handle;
        }

        public void ChangeSkin(string skinName)
        {
            Skeleton.SetSkin(skinName);
            Skeleton.SetToSetupPose();
        }
        
        private void Handle(TrackEntry track)
        {
            Debug.Log($"tE.Animation.Name: {track.Animation.Name}");
        }
    }
}
