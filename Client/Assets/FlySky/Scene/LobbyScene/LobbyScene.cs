﻿using FlySky.Tool;
using UnityEngine;

namespace FlySky.Scene.LobbyScene
{
    public class LobbyScene : MonoBehaviour
    {
        public SpineUser spine;
        public string skinName = "p2";
        public string animName = "skill";
        public bool isLoop;
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log("A");
                spine.ChangeSkin(skinName);
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                Debug.Log("S");
                spine.SetAnimation(animName, isLoop);
            }
        }
    }
}
